/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QDebug>
#include <QtDBus>

#include "PowerAdaptor.hpp"
#include "PowerDaemon.hpp"

DesQPowerDaemon::DesQPowerDaemon() {

    mPowerMode = DesQPowerDaemon::PowerModeNormal;

    new PowerAdaptor( this );

	QDBusConnection dbus = QDBusConnection::sessionBus();
    objectRegistered = dbus.registerObject( "/org/DesQ/Power", this );
    serviceRunning = dbus.registerService( "org.DesQ.Power" );

    QDBusInterface session( "org.DesQ.Session", "/org/DesQ/Session", "org.DesQ.Session", dbus );
    session.call( "RegisterService", "org.DesQ.Power" );

    upower = new QDBusInterface(
        "org.freedesktop.UPower",
        "/org/freedesktop/UPower",
        "org.freedesktop.UPower",
        QDBusConnection::systemBus()
    );

    display = new QDBusInterface(
        "org.freedesktop.UPower",
        "/org/freedesktop/UPower/devices/DisplayDevice",
        "org.freedesktop.UPower.Device",
        QDBusConnection::systemBus()
    );

    upower->call( "Refresh" );

    mLidOpen = not upower->property( "LidIsOpen" ).toBool();
    mOnBattery = upower->property( "OnBattery" ).toBool();
    mLastPercentage = display->property( "Percentage" ).toDouble();
};


bool DesQPowerDaemon::isServiceRunning() {

	return serviceRunning;
};

bool DesQPowerDaemon::isObjectRegistered() {

	return objectRegistered;
};

void DesQPowerDaemon::startManagement() {

    updateDevices();

    /* Battery/PowerSupply changes */
    QDBusConnection::systemBus().connect(
        "org.freedesktop.UPower",
        "/org/freedesktop/UPower/devices/DisplayDevice",
        "org.freedesktop.DBus.Properties",
        "PropertiesChanged",
        this,
        SLOT( handlePowerChanges( QString, QVariantMap, QStringList ) )
    );

    /* For LidClosed/OnBattery changes */
    QDBusConnection::systemBus().connect(
        "org.freedesktop.UPower",
        "/org/freedesktop/UPower",
        "org.freedesktop.DBus.Properties",
        "PropertiesChanged",
        this,
        SLOT( handlePowerChanges( QString, QVariantMap, QStringList ) )
    );

    /* Power Device Added */
    QDBusConnection::systemBus().connect(
        "org.freedesktop.UPower",
        "/org/freedesktop/UPower",
        "org.freedesktop.UPower",
        "DeviceAdded",
        this,
        SLOT( deviceAdded( QDBusObjectPath ) )
    );

    /* Power Device Removed */
    QDBusConnection::systemBus().connect(
        "org.freedesktop.UPower",
        "/org/freedesktop/UPower",
        "org.freedesktop.UPower",
        "DeviceRemoved",
        this,
        SLOT( deviceRemoved( QDBusObjectPath ) )
    );
};


bool DesQPowerDaemon::OnBattery() {

    return mOnBattery;
};

double DesQPowerDaemon::BatteryCharge() {

    /* Some batteries don't report data unless refreshed */
    upower->call( "Refresh" );

    /* Update mLastPercentage */
    mLastPercentage = display->property( "Percentage" ).toDouble();
    return mLastPercentage;
};

void DesQPowerDaemon::handleOnBattery() {

    mETR = display->property( "TimeToEmpty" ).toLongLong();

    for( QString dev: devices.keys() ) {
        if ( devices[ dev ]->type == Device::DeviceBattery ) {
            if ( devices[ dev ]->state == Device::DeviceStateDischarging ) {
                qDebug() << "Device running on battery.";
                qDebug() << devices[ dev ]->name << "is discharging.";
                qDebug() << "Estimated time remaining" << devices[ dev ]->timeToEmpty << "seconds";
            }
        }

        else if ( devices[ dev ]->type == Device::DeviceUps ) {
            qDebug() << "Device running on UPS.";
            qDebug() << devices[ dev ]->name << "is discharging.";
            qDebug() << "Estimated time remaining" << devices[ dev ]->timeToEmpty << "seconds";
        }
    }
};

void DesQPowerDaemon::handleOnACPower() {

    qDebug() << "AC Power Restored. Batteries/UPS charging.";
};

void DesQPowerDaemon::handleLidClosed() {

    qDebug() << "Lid was closed. Shall we suspend our system?";
};

void DesQPowerDaemon::handleLidOpened() {

    qDebug() << "Lid was opened. Anything special to do now?";
};

void DesQPowerDaemon::updateDevices() {

    devices.clear();

    QStringList result;

    QDBusInterface iface( "org.freedesktop.UPower", "/org/freedesktop/UPower/devices", "org.freedesktop.DBus.Introspectable", QDBusConnection::systemBus() );
    QDBusPendingReply<QString> reply = iface.call( "Introspect" );

    if ( reply.isError() ) {
        qWarning() << "Failed to fetch devices";
        return;
    }

    QXmlStreamReader xml( reply.value() );
    if ( xml.hasError() ) {
        qDebug() << "Failed to parse devices XML.";
        return;
    }

    QList<QDBusObjectPath> objects;
    while ( !xml.atEnd() ) {
        xml.readNext();
        if ( ( xml.tokenType() == QXmlStreamReader::StartElement ) && ( xml.name().toString() == "node" ) ) {
            QString name = xml.attributes().value( "name" ).toString();
            if ( not name.isEmpty() ) {
                QString path( "/org/freedesktop/UPower/devices/" + name );
                if ( devices.contains( path ) )
                    continue;

                qDebug() << "Added device" << name;
                Device *newDevice = new Device( path, this );
                connect( newDevice, &Device::deviceChanged, this, &DesQPowerDaemon::deviceChanged );
                devices[ path ] = newDevice;
            }
        }
    }
};

void DesQPowerDaemon::deviceChanged( QString path ) {

    qDebug() << path << "changed";
};

void DesQPowerDaemon::handlePowerChanges( QString interface, QVariantMap valueMap, QStringList ) {

    qDebug() << "   " << "Interface" << interface;
    qDebug() << "   " << "ValueMap " << valueMap.keys();

    /* Check for Lid State Changes */
    bool lidOpen = not upower->property( "LidIsClosed" ).toBool();
    if ( lidOpen != mLidOpen ) {
        mLidOpen = lidOpen;
        if ( mLidOpen ) {
            emit LidOpened();
            handleLidOpened();
        }

        else {
            emit LidClosed();
            handleLidClosed();
        }
    }

    /* Some batteries don't report data unless refreshed */
    upower->call( "Refresh" );

    /* Check for Power State Changes */
    bool battery = upower->property( "OnBattery" ).toBool();
    if ( battery != mOnBattery ) {
        mOnBattery = battery;
        if ( mOnBattery ) {
            emit SwitchedToBattery();
            handleOnBattery();
        }

        else {
            emit SwitchedToACPower();
            handleOnACPower();

            /* Reset the signalled flags when charging */
            /* This way, user is informed again when the power is lost */
            mSignalled60 = false;
            mSignalled300 = false;
        }
    }

    mETR = display->property( "TimeToEmpty" ).toLongLong();
    mETF = display->property( "TimeToFull" ).toLongLong();

    /* Update ETR and ETF */
    if ( mOnBattery ) {

        /* If mETR is greater than 60 / 300, reset those flags */
        if ( mETR > 60 )
            mSignalled60 = false;

        if ( mETR > 300 )
            mSignalled300 = false;

        /* If only around 1m remains, intimate the user */
        /* Take CriticalAction */
        if ( ( mETR <= 60 ) and ( not mSignalled60 ) ) {
            emit BatteryEmpty();
            mSignalled60 = true;
        }

        /* If only around 5m remains, intimate the user */
        /* Take CriticalAction */
        else if ( ( mETR <= 300 ) and ( not mSignalled300 ) ) {
            emit BatteryNearlyEmpty();
            mSignalled300 = true;
        }
    }

    /* Battery percentage */
    double curBatPC = display->property( "Percentage" ).toDouble();
    if ( mLastPercentage != curBatPC ) {
        mLastPercentage = curBatPC;
        qDebug() << "Battery Charge:" << mLastPercentage;
        emit BatteryChargeChanged( mLastPercentage );
        if ( mLastPercentage == 100 )
            emit BatteryFullyCharged();
    }
};

void DesQPowerDaemon::deviceAdded( QDBusObjectPath ) {

    updateDevices();
};

void DesQPowerDaemon::deviceRemoved( QDBusObjectPath ) {

    updateDevices();
};
