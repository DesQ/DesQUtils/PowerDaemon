/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <QObject>
#include <QtDBus>
#include <qcontainerfwd.h>

class PowerAdaptor: public QDBusAbstractAdaptor {

    Q_OBJECT;

    Q_CLASSINFO( "D-Bus Interface", "org.DesQ.Power" );
    Q_CLASSINFO( "D-Bus Introspection", ""
        "<interface name=\"org.DesQ.Power\">\n"
        "   <signal name=\"LidOpened\" />\n"
        "   <signal name=\"LidClosed\" />\n"
        "   <signal name=\"SwitchedToBattery\" />\n"
        "   <signal name=\"SwitchedToACPower\" />\n"
        "   <signal name=\"BatteryFullyCharged\" />\n"
        "   <signal name=\"BatteryNearlyEmpty\" />\n"
        "   <signal name=\"BatteryEmpty\" />\n"
        "   <signal name=\"BatteryChargeChanged\">\n"
        "       <arg direction=\"out\" type=\"d\" name=\"value\"/>\n"
        "   </signal>\n"
        "   <method name=\"OnBattery\">\n"
        "       <arg direction=\"out\" type=\"b\"/>\n"
        "   </method>\n"
        "   <method name=\"BatteryCharge\">\n"
        "       <arg direction=\"out\" type=\"d\"/>\n"
        "   </method>\n"
        "</interface>\n"
    "");

    public:
        PowerAdaptor(QObject *parent);
        virtual ~PowerAdaptor();

    public Q_SLOTS:
        bool OnBattery();
        double BatteryCharge();

    Q_SIGNALS:

		/* Lid Open/Close Signals */
		void LidOpened();
		void LidClosed();

		/* Battery Charge/Discharge Signals */
		void SwitchedToBattery();
		void SwitchedToACPower();

		/* Battery charge changed */
		void BatteryChargeChanged( double );

		/* Battery full signal */
		void BatteryFullyCharged();

		/* Battery nearly (5m) and almost (1m) empty */
		void BatteryNearlyEmpty();
		void BatteryEmpty();
};
