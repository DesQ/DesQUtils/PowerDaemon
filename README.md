# DesQ Power Daemon
Power Daemon is the executable that watches the power status and performs actions based on the user's choices

### Notes for compiling (Qt5) - linux:

- Get the sources
  - `git clone https://gitlab.com/DesQ/DesQUtils/PowerDaemon`
- Enter the `PowerDaemon` and create and enter `build`.
  - `cd PowerDaemon && mkdir build && cd build`
- Configure the project - we use cmake for project management
  - `cmake ..`
- Compile and install - we use make
  - `make -kj$(nproc) && sudo make install`

### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)

## My System Info
* OS:				Debian Sid
* Qt:				Qt5 5.15.1

### Known Bugs
* Please test and let me know

### Upcoming
* Any other feature you request for... :)
