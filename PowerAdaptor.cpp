/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "PowerAdaptor.hpp"
#include <QtCore/QMetaObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

PowerAdaptor::PowerAdaptor(QObject *parent) : QDBusAbstractAdaptor(parent) {

    setAutoRelaySignals( true );
};

PowerAdaptor::~PowerAdaptor() {

};

bool PowerAdaptor::OnBattery() {

    bool yes;
    QMetaObject::invokeMethod( parent(), "OnBattery", Q_RETURN_ARG( bool, yes ) );

    return yes;
};

double PowerAdaptor::BatteryCharge() {

    double value;
    QMetaObject::invokeMethod( parent(), "BatteryCharge", Q_RETURN_ARG( double, value ) );

    return value;
};
