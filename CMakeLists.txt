project( desq-power-daemon )
cmake_minimum_required( VERSION 3.8 )

set( PROJECT_VERSION 1.0.0 )
set( PROJECT_VERSION_MAJOR 1 )
set( PROJECT_VERSION_MINOR 0 )
set( PROJECT_VERSION_PATCH 0 )

set( PROJECT_VERSION_MAJOR_MINOR ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR} )
add_compile_definitions(VERSION_TEXT="${PROJECT_VERSION}")

set( CMAKE_CXX_STANDARD 17 )
set( CMAKE_INCLUDE_CURRENT_DIR ON )
set( CMAKE_BUILD_TYPE Release )

add_definitions ( -Wall )
if ( CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT )
    set( CMAKE_INSTALL_PREFIX "/usr" CACHE PATH "Location for installing the project" FORCE )
endif()

if ( CMAKE_INSTALL_LIBDIR STREQUAL "" OR NOT DEFINED CMAKE_INSTALL_LIBDIR )
    message( "-- CMAKE_INSTALL_LIBDIR not set. Trying pkg-config" )
    find_package( PkgConfig REQUIRED )
    pkg_get_variable( LIBDIR desq-core libdir )
    set( CMAKE_INSTALL_LIBDIR ${LIBDIR} )

    if ( LIBDIR STREQUAL "" OR NOT DEFINED LIBDIR )
        message( "-- Unable to retrieve information from pkg-config." )
        set( CMAKE_INSTALL_LIBDIR "lib/x86_64-linux-gnu" )
        message( "-- Using default value for CMAKE_INSTALL_LIBDIR: ${CMAKE_INSTALL_LIBDIR}" )
    endif()
endif()

if ( NOT DEFINED PKGCONFPATH )
    message( "-- PKGCONFPATH not set. Trying pkg-config" )
    find_package( PkgConfig REQUIRED )
    pkg_get_variable( PKGCONFPATH desq-core pkgconfpath )

    if ( PKGCONFPATH STREQUAL "" OR NOT DEFINED PKGCONFPATH )
        message( "-- Unable to retrieve the value of PKGCONFPATH." )
        set( PKGCONFPATH "/etc/xdg/desq" )
        message( "-- Using default value: /etc/xdg/desq" )
    endif()
endif()

set( CMAKE_AUTOMOC ON )
set( CMAKE_AUTORCC ON )
set( CMAKE_AUTOUIC ON )

find_package ( Qt5Core REQUIRED )
find_package ( Qt5DBus REQUIRED )

include_directories( lib )

set ( HDRS
	PowerAdaptor.hpp
	PowerDaemon.hpp
	lib/device.h
)

set ( SRCS
	Main.cpp
	PowerAdaptor.cpp
	PowerDaemon.cpp
	lib/device.cpp
)

add_executable ( desq-power-daemon ${SRCS} ${HDRS} )
target_link_libraries ( desq-power-daemon  Qt5::Core Qt5::DBus )

configure_file( power-daemon.conf.in power-daemon.conf @ONLY )

# Main Executable
install( TARGETS desq-power-daemon DESTINATION ${CMAKE_INSTALL_LIBDIR}/libexec/desq )
install( FILES ${CMAKE_CURRENT_BINARY_DIR}/power-daemon.conf DESTINATION ${PKGCONFPATH} )
