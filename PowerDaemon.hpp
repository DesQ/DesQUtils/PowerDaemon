/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project ( https://gitlab.com/desq/ )
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include "device.h"

class DesQPowerDaemon : public QObject {
	Q_OBJECT

	public:
		enum PowerMode {
			PowerModeNormal				= 0x65B628,
			PowerModePerformance,
			PowerModeGaming,
			PowerModeLowBattery,
			PowerModeCriticalBattery
		};

		DesQPowerDaemon();

		bool isServiceRunning();
		bool isObjectRegistered();

		void startManagement();

	public Q_SLOTS:
        bool OnBattery();
        double BatteryCharge();

	private:
		QMap<QString, Device*> devices;

		/* UPower DBus Interface Objects */
		QDBusInterface *upower;
		QDBusInterface *display;

		/* Power Management Mode (Default: Normal) */
		PowerMode mPowerMode;

		/*  */
		bool serviceRunning;
		bool objectRegistered;

		/* State of the lid */
		bool mLidOpen;

		/* State of the power supply */
		bool mOnBattery;

		/* Last battery percentage */
		double mLastPercentage = 0;

		/* Estimated time remaining */
		int mETR = 0;
		bool mSignalled60 = false;
		bool mSignalled300 = false;

		/* Estimated time to full */
		int mETF = 0;

		void handleOnBattery();
		void handleOnACPower();

		void handleLidClosed();
		void handleLidOpened();

		void updateDevices();
		void deviceChanged( QString );

	private Q_SLOTS:
		void handlePowerChanges( QString, QVariantMap, QStringList );

		void deviceAdded( QDBusObjectPath );
		void deviceRemoved( QDBusObjectPath );

	Q_SIGNALS:

		/* Lid Open/Close Signals */
		void LidOpened();
		void LidClosed();

		/* Battery Charge/Discharge Signals */
		void SwitchedToBattery();
		void SwitchedToACPower();

		/* Battery charge changed */
		void BatteryChargeChanged( double );

		/* Battery full signal */
		void BatteryFullyCharged();

		/* Battery nearly (5m) and almost (1m) empty */
		void BatteryNearlyEmpty();
		void BatteryEmpty();
};
